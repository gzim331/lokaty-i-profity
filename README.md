## Instruction

### Set the data in data.txt:

- **percent** - deposit interest rate
- **durationDeposit** - duration of a single deposit (in months)
- **maxMonths** - maximum investment duration (in months)
- **amountStart** - startup assets
- **suplement** - the amount of assets deposited monthly during the term of the deposit
- **tax** - profit tax

### Run project
To run the project, you must have a php interpreter (tested on php8.1).
For this purpose, you can use an already prepared environment built on Docker

```
cd path/to/project
docker-compose up -d --build
docker exec -ti profit-php php profit.php
```