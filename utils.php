<?php

function getDecimal(float $number)
{
    return floor(($number) * 100) / 100;
}

function getDataFromFile()
{
    $file_content = file_get_contents('data.txt');
    $lines = explode(PHP_EOL, $file_content);
    $data = [];

    foreach ($lines as $line) {
        list($key, $value) = explode('=', $line);

        $key = trim($key);
        $value = trim($value);

        if (str_contains($value, '.')) {
            $data[$key] = (float)$value;
        } else {
            $data[$key] = (int)$value;
        }
    }

    try {
        dataFromFileValidation($data);
    } catch (Exception $e) {
        echo $e->getMessage();
        die;
    }

    return $data;
}

function dataFromFileValidation(array $data)
{
    $errors = '';

    if (!key_exists('percent', $data)) {
        $errors .= "'percent' doesn't exists\n";
    }
    if (!is_float($data['percent'])) {
        $errors .= "'percent' isn't float\n";
    }

    if (!key_exists('durationDeposit', $data)) {
        $errors .= "'durationDeposit' doesn't exists\n";
    }
    if (!is_int($data['durationDeposit'])) {
        $errors .= "'durationDeposit' isn't integer\n";
    }
    if ($data['durationDeposit'] <= 0) {
        $errors .= "'durationDeposit' can't be less than or equal to 0\n";
    }

    if (!key_exists('maxMonths', $data)) {
        $errors .= "'maxMonths' doesn't exists\n";
    }
    if (!is_int($data['maxMonths'])) {
        $errors .= "'maxMonths' isn't integer\n";
    }
    if ($data['maxMonths'] <= 0) {
        $errors .= "'maxMonths' can't be less than or equal to 0\n";
    }

    if (!key_exists('amountStart', $data)) {
        $errors .= "'amountStart' doesn't exists\n";
    }
    if ($data['amountStart'] <= 0) {
        $errors .= "'amountStart' can't be less than or equal to 0\n";
    }

    if (!key_exists('suplement', $data)) {
        $errors .= "'suplement' doesn't exists\n";
    }
    if ($data['suplement'] < 0) {
        $errors .= "'suplement' can't be less than 0\n";
    }

    if (!key_exists('tax', $data)) {
        $errors .= "'tax' doesn't exists\n";
    }
    if ($data['tax'] < 0) {
        $errors .= "'tax' can't be less than 0\n";
    }

    if (!empty($errors)) {
        $message = "Problem with data.txt:\n" . $errors;
        throw new Exception($message);
    }
}