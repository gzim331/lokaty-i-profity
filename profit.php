<?php

require_once 'utils.php';

$data = getDataFromFile();

$percent = $data['percent'];
$durationDeposit = $data['durationDeposit'];
$maxMonths = $data['maxMonths'];
$amountStart = $data['amountStart'];
$suplement = $data['suplement'];

$tax = $data['tax'];
$profits = 0;


$amount = $amountStart;
$amountStartDuration = $amountStart;

for ($i = 0; $i < $maxMonths; $i += $durationDeposit) {
    $profitPerDeposit = 0;

    for ($j = 0; $j < $durationDeposit; $j++) {
        $profitBruttoPerYear = getDecimal($amount * $percent);
        $profit = getDecimal($profitBruttoPerYear / 12);
        $profitPerDeposit += $profit;
        $amount += $suplement;
    }

    $profitNettoPerDeposit = getDecimal($profitPerDeposit - $profitPerDeposit * $tax);
    $amount += $profitNettoPerDeposit;
    $profits += $profitPerDeposit;
}

echo 'Profit: ' . $profits . PHP_EOL;
echo 'Amount: ' . $amount . PHP_EOL;
